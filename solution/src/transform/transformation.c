#include "../../include/transformation.h"
#include <stdint.h>


struct image rotate90left(struct image const source) {
	struct image rotated_image = new_image(source.height, source.width);
  if (rotated_image.data != NULL) {
	  for (uint64_t i = 0; i < rotated_image.height; ++i) {
		  for (uint64_t j = 0; j < rotated_image.width; ++j) {
		    rotated_image.data[rotated_image.width * i + j] = source.data[(source.height - 1 - j) * source.width + i];
		  }
	  }
	  return rotated_image;
  }
  return (struct image) {0};
}

struct image rotate90right(struct image const source) {
  struct image rotated_image = new_image(source.height, source.width);
  if (rotated_image.data != NULL) {                                                   
    for (uint64_t i = 0; i < rotated_image.height; ++i) {                                                                  
      for (uint64_t j = 0; j < rotated_image.width; ++j) {                                                           
        rotated_image.data[rotated_image.width * i + j] = source.data[(source.width) * j + (source.width - 1 - i)];   
      }                                                                                                              
    }                                                                                                                      
    return rotated_image;
  }
  return (struct image) {0};                                                                                                  
}
