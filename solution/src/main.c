#include "../include/main.h"
#include "../include/file_handler.h"
#include "../include/image_format.h"
#include "../include/serializer.h"
#include "../include/transformation.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {

    if (argc != 3) {
        printf("Wrong number of arguments");
        return -1;
    }
    
    FILE *input = NULL, *output = NULL;
    
    if (open_file(&input, argv[1], "r") == OPEN_OK) {
        printf("Input file successfully opened\n");
    }
    else {
        printf("Error in opening the input file");
        return -1;
    }

    if (open_file(&output, argv[2], "w") == OPEN_OK) {
        printf("Output file successfully opened\n");
    }
    else {
        printf("Error in opening the output file");
	    return -1;
    }

    struct image* old = malloc(sizeof(struct image));

    enum read_status file_read_status = from_bmp(input, old);
    switch (file_read_status) {
        case READ_OK:
            printf("The image is successfully read\n");
            break;
        case READ_INVALID_SIGNATURE:
            printf("Eror in BMP file signature");
	        delete_image(old);
	        return -1;
        case READ_INVALID_BITS:
            printf("Error in BMP file bits");
	        delete_image(old);
	        return -1;
        case READ_INVALID_HEADER:
            printf("Error in BMP file header");
            delete_image(old);
	        return -1;
        case READ_NULL_IMAGE:
            printf("Error in reading image");
            delete_image(old);
	        return -1;    
        case READ_ERR:
            printf("Something else was wrong");
            delete_image(old);
            return -1;
    }

    struct image* rotated = malloc(sizeof(struct image));

    if (!rotated) {
        delete_image(old);
        printf ("Something went wrong");
        return -1;
    }

    *rotated = rotate90left(*old);
    delete_image(old);
    old = NULL;

    if (rotated->data == NULL) {
        printf ("Something went wrong in rotating");
        delete_image(rotated);
        return -1;
    }
    printf("The image successfully rotated\n");

    if (to_bmp(output, rotated) == WRITE_OK) {
        printf("The rotated image successfully written\n");
    }
    else {
        printf("Error in writting rotated image");
	    delete_image(rotated);
	    return -1;
    }

    delete_image(rotated);
    rotated = NULL;


    if (close_file(input) == CLOSE_OK) {
        printf("Input file successfully closed\n");
    }
    else {
        printf("Error in closing the input file");
	return -1;
    }

    if (close_file(output) == CLOSE_OK) {
        printf("Output file successfully closed\n");
    }
    else {
        printf("Error in closing the output file");
	return -1;
    }
    
    return 0;
}
