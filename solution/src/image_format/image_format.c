#include "../../include/image_format.h"

struct image new_image(const uint64_t width, const uint64_t height) {
	return (struct image) {
		.width = width,
			.height = height,
				 .data = (struct pixel *) malloc(sizeof(struct pixel) * width * height)
	};
}

void delete_image(struct image* restrict image_for_delete) {
	if (image_for_delete) {
		free(image_for_delete->data);
		image_for_delete->data = NULL;
		free(image_for_delete);
	}
}
