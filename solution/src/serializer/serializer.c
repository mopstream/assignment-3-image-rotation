#include "../../include/bmp_header.h"  
#include "../../include/image_format.h"
#include "../../include/serializer.h"
   
#include <inttypes.h>
#include <stdio.h>

enum {
	BMP_BF_TYPE = 0x4d42,
	BMP_BI_BIT_COUNT = 24,
	BMP_BF_RESERVED = 0,
	BMP_BI_SIZE = 40,
	BMP_BI_PLANES = 1,
	BMP_BI_COMPRESSION = 0,
	BMP_BI_X_PELS_PER_METER = 0,
	BMP_BI_Y_PELS_PER_METER = 0,
	BMP_BI_CLR_USED = 0,
	BMP_BI_CLR_IMPORTANT = 0,
	BMP_PIXEL_SIZE = sizeof(struct pixel),
	BMP_HEADER_SIZE = sizeof(struct bmp_header)
};

static inline int64_t get_padding(const int64_t width) {
	return (4 - (width * 3) % 4) % 4;
}

enum read_status from_bmp(FILE* restrict in, struct image* restrict img) {
	if (!in || !img) return READ_NULL_IMAGE;
	
	struct bmp_header header;

	if (!fread(&header, BMP_HEADER_SIZE, 1, in)) {
		return READ_INVALID_HEADER;
	}

	if (header.bfType != BMP_BF_TYPE) {
		return READ_INVALID_SIGNATURE;
	}

	if (header.biBitCount != BMP_BI_BIT_COUNT) {
		return READ_INVALID_BITS;
	}

	*img = new_image(header.biWidth, header.biHeight);

	if (!img->data) return READ_ERR;

	int64_t padding = get_padding((int64_t)img->width);

	for (uint64_t i = 0; i < img->height; ++i) {
	  if (img->width != fread(img->data + img->width * i, BMP_PIXEL_SIZE, img->width, in)) {
		  	delete_image(img);
			return READ_ERR;
		}

		if (fseek(in, padding, SEEK_CUR)) {
			delete_image(img);
			return READ_ERR;
		}
	}
	return READ_OK;
}

enum write_status to_bmp(FILE* restrict out, struct image* restrict img) {
	if (!out || !img) return WRITE_ERROR;
	
	int64_t padding = get_padding((int64_t)img->width);

	struct bmp_header header = (struct bmp_header){
		.bfType = BMP_BF_TYPE,
		.bfileSize = img->height * (img->width + padding) * BMP_PIXEL_SIZE + BMP_HEADER_SIZE,
		.bfReserved = BMP_BF_RESERVED,
		.bOffBits = BMP_HEADER_SIZE,
		.biSize = BMP_BI_SIZE,
		.biWidth = img->width,
		.biHeight = img->height,
		.biPlanes = BMP_BI_PLANES,
		.biBitCount = BMP_BI_BIT_COUNT,
		.biCompression = BMP_BI_COMPRESSION,
		.biSizeImage = img->height * (img->width + padding) * BMP_PIXEL_SIZE,
		.biXPelsPerMeter = BMP_BI_X_PELS_PER_METER,
		.biYPelsPerMeter = BMP_BI_Y_PELS_PER_METER,
		.biClrUsed = BMP_BI_CLR_USED,
		.biClrImportant = BMP_BI_CLR_IMPORTANT
	};
	
	if (!fwrite(&header, BMP_HEADER_SIZE, 1, out)) {
		return WRITE_ERROR;
	}

	for (uint64_t i = 0; i < img->height; ++i) {
		if (!fwrite(img->data + i * img->width, BMP_PIXEL_SIZE, img->width, out)) {
			return WRITE_ERROR;
		}

		uint8_t garbage[3] = { 0 };

		if (!fwrite(&garbage, padding, 1, out) && (padding != 0)) {
			return WRITE_ERROR;
		}
	}
	return WRITE_OK;
}
 
