#include "../../include/file_handler.h"
#include <stdio.h>

enum open_status open_file(FILE** restrict file, char const* restrict fname, char const* restrict mode) {
	if (!file || !fname || !mode) return OPEN_ERR;
	*file = fopen(fname, mode);
	if (*file == NULL) {
	  return OPEN_ERR;
	}
	return OPEN_OK;
}

enum close_status close_file(FILE* restrict file) {
	if (fclose(file) == 0) {
		return CLOSE_OK;
	}
	return CLOSE_ERR;
}
