#ifndef IMAGE_FORMAT_HEADER
#define IMAGE_FORMAT_HEADER

#include <inttypes.h>
#include <stdlib.h>

struct __attribute__((packed)) image {
	uint64_t width, height;
	struct pixel* data;
};

struct __attribute__((packed)) pixel {
	uint8_t b, g, r;
};

struct image new_image(uint64_t width, uint64_t height);

void delete_image(struct image* restrict image_for_delete);
#endif
