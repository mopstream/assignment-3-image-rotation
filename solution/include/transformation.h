#ifndef TRANSFORM_HEADER
#define TRANSFORM_HEADER

#include "image_format.h"

struct image rotate90left(struct image const source);

struct image rotate90right(struct image const source);

#endif
