#ifndef FILE_HANDLER_HEADER
#define FILE_HANDLER_HEADER

#include <stdio.h>

enum open_status {
	OPEN_OK = 0,
	OPEN_ERR
};

enum close_status {
	CLOSE_OK = 0,
	CLOSE_ERR
};

enum open_status open_file(FILE** restrict file, char const* restrict fname, char const* restrict mode);

enum close_status close_file(FILE* restrict file);

#endif
