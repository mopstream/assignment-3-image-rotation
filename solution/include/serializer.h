#ifndef SERIALIZR_HEADER
#define SERIALIZR_HEADER

#include "image_format.h"
#include <stdio.h>

enum read_status {
	READ_OK = 0,
	READ_INVALID_SIGNATURE,
	READ_INVALID_BITS,
	READ_INVALID_HEADER,
	READ_NULL_IMAGE,
	READ_ERR
};

enum  write_status {
	WRITE_OK = 0,
	WRITE_ERROR
};

enum read_status from_bmp(FILE* restrict in, struct image* restrict img);

enum write_status to_bmp(FILE* restrict out, struct image* restrict img);

#endif
