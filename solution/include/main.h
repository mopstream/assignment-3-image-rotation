#ifndef MAIN_HEADER
#define MAIN_HEADER

#include <stdio.h>

void free_files(FILE** restrict fi, FILE** restrict se);

int main(int argc, char** argv);

#endif
